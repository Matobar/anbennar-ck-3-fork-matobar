﻿name_list_corvurian = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {
		Adrian Alexandru Anghel Aron Bajan Balc Barbat Basarab Bogdan Carol Ciprian Ciubar Claudiu
		Corneliu Costin Damjan Dan Dragos Dumitru Emerik Florin Franjo Gavril Gheorghe Grigore Iacob
		Iancu Ieremia Ilie Ioan Iorghu Iosif Iuga Janos Ladislau Latcu Laurentiu Litovoi Lucian Marin
		Mihai Milos Mircea Moise Nicolaie Petre Pirvu Radovan Radu Sas Seneslav Sergiu Simion
		Stefan Stelian Teodor Tepes Tibor Tihomir Timotei Tudor Valentin Valeriu Vasile Veaceslav
		Victor Vilhelm Vintila Vlad
	}
	female_names = {
		Adelina Adriana Afina Alexandra Alexia Ana Anastasia Angela Arina Clara Dana Ecatarina
		Elena Elisabeta Emilia Eufroysina Felicia Floarea Iacoba Ioana Ionela Irina Iulia Maria Monica
		Natalia Olimpia Paraschiva Petra Roxana Ruxandra Smaranda Sophia Stana Stefana Stefania Teodora
		Tereza Vasilica Violeta Voica Zina
		
	}

	
	dynasty_of_location_prefix = "dynnp_sil"
	
	patronym_suffix_male = "dynnpat_suf_escu"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 40
	mat_grf_name_chance = 10
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 40
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_korbarid = {

	cadet_dynasty_names = {
		"dynn_Abkanid"
		"dynn_Akamonid"
		"dynn_Aldanid"
		"dynn_Alononid"
		"dynn_Amolustanid"
		"dynn_Balakurnid"
		"dynn_Bikilnid"
		"dynn_Boganid"
		"dynn_Boranid"
		"dynn_Brincosnid"
		"dynn_Britosnid"
		"dynn_Bruclanid"
		"dynn_Burisnid"
		"dynn_Buzasmurgenid"
		"dynn_Buzasnid"
		"dynn_Comosicanid"
		"dynn_Copacburnid"
		"dynn_Daravanid"
		"dynn_Davanid"
		"dynn_Dengisnid"
		"dynn_Didanid"
		"dynn_Diriganid"
		"dynn_Doinanid"
		"dynn_Dorpokinid"
		"dynn_Ebrosnid"
		"dynn_Ebrosnid"
		"dynn_Eskaivasnid"
		"dynn_Eskaudnid"
		"dynn_Eskrumbnid"
		"dynn_Estelenid"
		"dynn_Estrunganid"
		"dynn_Genurnid"
		"dynn_Gernid"
		"dynn_Geskaivasnid"
		"dynn_Gilusinid"
		"dynn_Giranid"
		"dynn_Gujirusnid"
		"dynn_Hiutanid"
		"dynn_Imbracanid"
		"dynn_Indemanid"
		"dynn_Kaganid"
		"dynn_Karnid"
		"dynn_Kedintanid"
		"dynn_Kersanid"
		"dynn_Kodamanid"
		"dynn_Komozinid"
		"dynn_Kortirnid"
		"dynn_Lumianid"
		"dynn_Marnid"
		"dynn_Mendrutanid"
		"dynn_Mirmosnid"
		"dynn_Moskonid"
		"dynn_Mosnid"
		"dynn_Motocnid"
		"dynn_Mucadornid"
		"dynn_Mulintikapnid"
		"dynn_Naskaivasnid"
		"dynn_Nicharnid"
		"dynn_Palmanid"
		"dynn_Razabocinid"
		"dynn_Riskaivasnid"
		"dynn_Rochisnid"
		"dynn_Rualnid"
		"dynn_Rudakalasnid"
		"dynn_Rudasnid"
		"dynn_Sampenid"
		"dynn_Saranid"
		"dynn_Semanid"
		"dynn_Sermamernid"
		"dynn_Suditautanid"
		"dynn_Tirunid"
		"dynn_Visunid"
		"dynn_Zalmonid"
		"dynn_Zenanid"
		"dynn_Zolaturnid"
	}



	dynasty_names = {
		"dynn_Abkanid"
		"dynn_Akamonid"
		"dynn_Aldanid"
		"dynn_Alononid"
		"dynn_Amolustanid"
		"dynn_Balakurnid"
		"dynn_Bikilnid"
		"dynn_Boganid"
		"dynn_Boranid"
		"dynn_Brincosnid"
		"dynn_Britosnid"
		"dynn_Bruclanid"
		"dynn_Burisnid"
		"dynn_Buzasmurgenid"
		"dynn_Buzasnid"
		"dynn_Comosicanid"
		"dynn_Copacburnid"
		"dynn_Daravanid"
		"dynn_Davanid"
		"dynn_Dengisnid"
		"dynn_Didanid"
		"dynn_Diriganid"
		"dynn_Doinanid"
		"dynn_Dorpokinid"
		"dynn_Ebrosnid"
		"dynn_Ebrosnid"
		"dynn_Eskaivasnid"
		"dynn_Eskaudnid"
		"dynn_Eskrumbnid"
		"dynn_Estelenid"
		"dynn_Estrunganid"
		"dynn_Genurnid"
		"dynn_Gernid"
		"dynn_Geskaivasnid"
		"dynn_Gilusinid"
		"dynn_Giranid"
		"dynn_Gujirusnid"
		"dynn_Hiutanid"
		"dynn_Imbracanid"
		"dynn_Indemanid"
		"dynn_Kaganid"
		"dynn_Karnid"
		"dynn_Kedintanid"
		"dynn_Kersanid"
		"dynn_Kodamanid"
		"dynn_Komozinid"
		"dynn_Kortirnid"
		"dynn_Lumianid"
		"dynn_Marnid"
		"dynn_Mendrutanid"
		"dynn_Mirmosnid"
		"dynn_Moskonid"
		"dynn_Mosnid"
		"dynn_Motocnid"
		"dynn_Mucadornid"
		"dynn_Mulintikapnid"
		"dynn_Naskaivasnid"
		"dynn_Nicharnid"
		"dynn_Palmanid"
		"dynn_Razabocinid"
		"dynn_Riskaivasnid"
		"dynn_Rochisnid"
		"dynn_Rualnid"
		"dynn_Rudakalasnid"
		"dynn_Rudasnid"
		"dynn_Sampenid"
		"dynn_Saranid"
		"dynn_Semanid"
		"dynn_Sermamernid"
		"dynn_Suditautanid"
		"dynn_Tirunid"
		"dynn_Visunid"
		"dynn_Zalmonid"
		"dynn_Zenanid"
		"dynn_Zolaturnid"
	}

	
	
	male_names = {
		Amolustan Balan Baliki Bastis BlegiS_an Blesan Bogan Boran Burebistan Carnabon Comosican DadA_zI_ DA_labasan
		Damanis DansI_mel DapI_ Daravan Dardilan Dekinan Dengis Didan Dirigan DI_rzun Diurpan Dolocher Dordan
		Dorpokis Dotos Dotuzi Duras Espirusur Estejar GebelizI_s Gebucu Gudilan KamoliuT_I_ Karn Kolvan Komozi
		KotiS_on Moskon Mucador Nichmer Oroles Petipor RaT_ibidan Rechmer Resturmer RochI_s Ruboster S_amarkos Saran 
		Sermamer T_iru ViS_ur Zalmos Zolatur
	}
	female_names = {
		AprusA_ Azilina BarasA_ Bikil BogA_ CoT_ela DadA_ DaS_ene Denzibal DeriA_ DirpanA_ Drilgisa DromiS_eta 
		DuS_idava Escoril ES_otia GebelizA_s Gerdaba GerisicA_ GiramA_r KagI_za KamoliuT_A_ KomakI_zA_ LunkA_ Mucatra NataporA_ 
		NichmA_r PuridorA_ RaT_ibidA_ RigozA_ S_adichen S_aper SermamarA_ Sina Tarbir Tiati Vezina ZalmosA_ Zia 
		ZiraS_es Zutula
	}
	
	dynasty_of_location_prefix = "dynnp_dan"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 40
	mat_grf_name_chance = 10
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 40
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}



name_list_cardesti = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {
		Adrian Alexandru Anghel Aron Bajan Balc Barbat Basarab Bogdan Carol Ciprian Ciubar Claudiu
		Corneliu Costin Damjan Dan Dragos Dumitru Emerik Florin Franjo Gavril Gheorghe Grigore Iacob
		Iancu Ieremia Ilie Ioan Iorghu Iosif Iuga Janos Ladislau Latcu Laurentiu Litovoi Lucian Marin
		Mihai Milos Mircea Moise Nicolaie Petre Pirvu Radovan Radu Sas Seneslav Sergiu Simion
		Stefan Stelian Teodor Tepes Tibor Tihomir Timotei Tudor Valentin Valeriu Vasile Veaceslav
		Victor Vilhelm Vintila Vlad
	}
	female_names = {
		Adelina Adriana Afina Alexandra Alexia Ana Anastasia Angela Arina Clara Dana Ecatarina
		Elena Elisabeta Emilia Eufroysina Felicia Floarea Iacoba Ioana Ionela Irina Iulia Maria Monica
		Natalia Olimpia Paraschiva Petra Roxana Ruxandra Smaranda Sophia Stana Stefana Stefania Teodora
		Tereza Vasilica Violeta Voica Zina
		
	}

	
	dynasty_of_location_prefix = "dynnp_sil"
	
	patronym_suffix_male = "dynnpat_suf_escu"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 40
	mat_grf_name_chance = 10
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 40
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}
