﻿give_corset_burial_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_personal_religious.dds"

	desc = give_corset_burial_decision_desc
	selection_tooltip = give_corset_burial_decision_tooltip

	is_shown = {
		faith = {
			has_doctrine_parameter = corset_burials_active
		}
		has_variable = ancestor_to_bury
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
	}

	effect = {
		show_as_tooltip = {
			add_piety = major_piety_value
			if = {
				limit = {
					any_vassal = {
						faith = {
							has_doctrine_parameter = corset_burials_active
						}
					}
				}
				every_vassal = {
					limit = {
						faith = {
							has_doctrine_parameter = corset_burials_active
						}
					}
					custom = give_corset_burial_vassals
					add_opinion = {
						modifier = pleased_opinion
						target = root
						opinion = 20
					}
				}
			}
		}
		trigger_event = anb_religious_decision.0001
	}

	ai_check_interval = 36
	
	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

recite_skaldic_tale_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_personal_religious.dds"
	desc = recite_skaldic_tale_desc
	selection_tooltip = recite_skaldic_tale_tooltip
	
	is_shown = {
		faith = {
			has_doctrine_parameter = skaldic_tales_active
		}
		has_skaldic_tale_active = no
		NOT = { has_variable = reciting_skaldic_tales }
	}
	
	is_valid = {
	}
	is_valid_showing_failures_only = {
	}
	
	cost = {
		piety = 100
	}
	
	effect = {
		show_as_tooltip = {
			if = {
				limit = {
					any_vassal = {
						faith = {
							has_doctrine_parameter = skaldic_tales_active
						}
					}
				}
				every_vassal = {
					limit = {
						faith = {
							has_doctrine_parameter = skaldic_tales_active
						}
					}
					custom = recite_skaldic_tale_vassals
					add_opinion = {
						modifier = pleased_opinion
						target = root
						opinion = 10
					}
				}
			}
		}
		
		hidden_effect = {
			set_variable = reciting_skaldic_tales
		}
		
		trigger_event = anb_religious_decision.0501
	}
	ai_check_interval = 36
	
	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}
select_personal_deity_eidoueni_decision = { # Anbennar
	picture = "gfx/interface/illustrations/decisions/decision_major_religion.dds"

	desc = select_personal_deity_eidoueni_decision_desc
	selection_tooltip = select_personal_deity_germanic_decision_tooltip

	is_shown = {
		religion = religion:lencori_religion
		faith = { has_doctrine_parameter = select_personal_god_active }
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
		# Have to at _least_ not be in piety debt.
		piety >= 0
	}

	cooldown = { years = 2 }

	effect = {
		# Show the possible options.
		show_as_tooltip = {
			random_list = {
				desc = select_personal_deity_eidoueni_decision_tt
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_artanos
					add_character_modifier = artanos_deity
				}
				100 = {
					trigger = { is_vaguely_danish_bhakti_trigger = no }
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_merisse
					add_character_modifier = merisse_deity
				}
				100 = {
					trigger = { is_vaguely_danish_bhakti_trigger = yes }
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_trovecos
					add_character_modifier = trovecos_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_careslobos
					add_character_modifier = careslobos_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_asmirethin
					add_character_modifier = asmirethin_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_damarta
					add_character_modifier = damarta_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_belouina
					add_character_modifier = belouina_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_dercanos
					add_character_modifier = dercanos_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_sorbodua
					add_character_modifier = sorbodua_deity

				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_turanos
					add_character_modifier = turanos_deity
				}
			}
		}
		# And the actual effect.
		trigger_event = anb_religious_decision.0104
	}

	ai_check_interval = 96
	
	ai_potential = {
		piety >= minor_piety_value
		NOR = {
			has_character_modifier = artanos_deity
			has_character_modifier = turanos_deity
			has_character_modifier = sorbodua_deity
			has_character_modifier = dercanos_deity
			has_character_modifier = belouina_deity
			has_character_modifier = asmirethin_deity
			has_character_modifier = careslobos_deity
			has_character_modifier = trovecos_deity
			has_character_modifier = merisse_deity
		}
	}

	ai_will_do = {
		base = 100
	}
}
