﻿
transfer_castanorian_general_titles = {
	if = {
		limit = {
			has_global_variable = accepted_castanorian_general
			exists = global_var:castanorian_general
			global_var:castanorian_general = {
				is_alive = yes
			}
		}
		show_as_tooltip = {
			global_var:castanorian_general = {
				create_title_and_vassal_change = {
					type = swear_fealty
					save_scope_as = change
					add_claim_on_loss = no
				}
				change_liege = {
					liege = scope:defender
					change = scope:change
				}
				resolve_title_and_vassal_change = scope:change
			}
			title:d_west_chillsbay = {
				create_title_and_vassal_change = {
					type = revoked
					save_scope_as = change
					add_claim_on_loss = yes
				}
				# Setup and execute the changes to titles and vassals.
				change_title_holder_include_vassals = {
					holder = global_var:castanorian_general
					change = scope:change
					government_base = scope:defender
				}
				resolve_title_and_vassal_change = scope:change
			}
		}
		
		hidden_effect = {
			create_title_and_vassal_change = {
				type = revoked
				save_scope_as = change
				add_claim_on_loss = yes
			}
			title:d_west_chillsbay = {
				change_title_holder_include_vassals = {
					holder = global_var:castanorian_general
					change = scope:change
					government_base = scope:defender
				}
			}
			title:c_alencay = {
				change_county_control = -40
				change_title_holder_include_vassals = {
					holder = global_var:castanorian_general
					change = scope:change
					government_base = scope:defender
				}
			}
			title:c_everwharf = {
				change_county_control = -40
				change_title_holder_include_vassals = {
					holder = global_var:castanorian_general
					change = scope:change
					government_base = scope:defender
				}
			}
			title:c_reachspier = {
				change_county_control = -40
				change_title_holder_include_vassals = {
					holder = global_var:castanorian_general
					change = scope:change
					government_base = scope:defender
				}
			}
			resolve_title_and_vassal_change = scope:change
			global_var:castanorian_general = {
				create_title_and_vassal_change = {
					type = swear_fealty
					save_scope_as = change
					add_claim_on_loss = no
				}
				change_liege = {
					liege = scope:defender
					change = scope:change
				}
				resolve_title_and_vassal_change = scope:change
			}
		}
	}
	
	cleanup_adshaw_revolt_variables = yes
}

transfer_maldorian_deland = {
	if = {
		limit = {
			has_global_variable = maldorian_joined
			exists = character:61
			character:61 = {
				is_alive = yes
			}
		}
		create_title_and_vassal_change = {
			type = conquest
			save_scope_as = change
			add_claim_on_loss = yes
		}
		title:c_deland = {
			change_title_holder_include_vassals = {
				holder = character:61
				change = scope:change
			}
			resolve_title_and_vassal_change = scope:change
		}
	}
	
	cleanup_adshaw_revolt_variables = yes
}

cleanup_adshaw_revolt_variables = {
	remove_global_variable = accepted_castanorian_general
	remove_global_variable = castanorian_general
	remove_global_variable = maldorian_joined
}
