﻿title_grant_is_tyrannical_trigger = {
	OR = {
		character_is_illegitimate_race_for_title = {
			TITLE = scope:landed_title
			CHARACTER = scope:recipient
		} 
	}
}