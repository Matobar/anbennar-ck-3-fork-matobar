﻿namespace = anb_reachman_independence

# Castanorian General Offers Help
anb_reachman_independence.0001 = {
	title = anb_reachman_independence.0001.title
	desc = anb_reachman_independence.0001.description
	theme = war
	
	left_portrait = { character = scope:castanorian_general animation = marshal }

	trigger = {
		global_var:castanorian_general ?= { # The general can't have died or have been landed
			is_alive = yes
			is_landed = no
		}
		
		title:k_adshaw.holder ?= {
			any_character_war = {
				using_cb = reachmman_independence_war
			}
		}
	}

	immediate = {
		global_var:castanorian_general = {
			save_scope_as = castanorian_general
		}
	}

	# Accept
	option = {
		name = anb_reachman_independence.0001.a

		add_prestige = minor_prestige_loss

		custom_tooltip = independent_reachmen_may_join_tooltip
		hidden_effect = {
			title:k_adshaw = {
				every_dejure_vassal_title_holder  = {
					add_prestige = minor_prestige_value
					trigger_event = {
						id = anb_reachman_independence.0002
						months = 1
					}
				}
			}
			
			set_global_variable = accepted_castanorian_general
		}

		custom_tooltip = castanorian_general_will_get_lands_tooltip

		spawn_army = {
			levies = 200
			
			men_at_arms = {
				type = armored_footmen
				stacks = 1
			}
			men_at_arms = {
				type = bowmen
				stacks = 1
			}
			
			location = capital_province
			inheritable = no
			name = castanorian_forces
		}
		
		global_var:castanorian_general = {
			add_hook = {
				type = indebted_hook
				target = ROOT
				years = 10
			}
		}

		hidden_effect = { set_employer = global_var:castanorian_general }
		add_courtier = global_var:castanorian_general

		ai_chance = {
			base = 75
		}
	}
	
	# Reject
	option = {
		name = anb_reachman_independence.0001.b
		
		add_prestige = minor_prestige_gain
		
		ai_chance = {
			base = 25
		}
	}
}

# Join the war?
anb_reachman_independence.0002 = {
	title = anb_reachman_independence.0002.title
	desc = anb_reachman_independence.0002.description
	theme = court

	left_portrait = { character = scope:adshaw_holder animation = triumphant }
	right_portrait = { character = global_var:castanorian_general animation = scheme }
	
	trigger = {
		is_independent_ruler = yes
		NOT = { has_truce = title:k_adshaw.holder }
		NOT = { is_at_war_with = title:k_adshaw.holder }
		NOT = { is_allied_to = title:k_adshaw.holder }
		NOT = { this = title:k_adshaw.holder }
		title:k_adshaw.holder ?= {
			any_character_war = {
				using_cb = reachmman_independence_war
			}
		}
	}

	immediate = {
		title:k_adshaw = {
			holder = {
				save_scope_as = adshaw_holder
			}
		}
	}

	# To Arms!
	option = {
		name = anb_reachman_independence.0002.a
		
		scope:adshaw_holder = {
			random_character_war = {
				limit = {
					using_cb = reachmman_independence_war
				}
				add_attacker = ROOT

			}
		}

		stress_impact = {
			arbitrary = minor_stress_impact_gain
			compassionate = medium_stress_impact_loss
		}
		
		set_global_variable = maldorian_joined
		
		ai_chance = {
			base = 25
			
			modifier = {
				opinion = {
					target = scope:adshaw_holder
					value >= 50
				}
				factor = 0
			}
			
			modifier = {
				add = -15
				is_at_war = yes
			}

			# If in debt don't join
			modifier = {
				debt_level >= 1
				add = {
					value = debt_level
					multiply = -5
				}
			}

			ai_value_modifier = {
				ai_boldness = 0.25
				ai_honor = 0.25
				ai_rationality = 0.25
			}

			opinion_modifier = {
				opinion_target = scope:adshaw_holder
				multiplier = -1 # May need to turn this up
			}
		}
	}

	# They will manage on their own
	option = {
		name = anb_reachman_independence.0002.b

		add_prestige = minor_prestige_value

		stress_impact = {
			brave = minor_stress_impact_gain
			craven = minor_stress_impact_loss
		}

		ai_chance = {
			base = 75

			ai_value_modifier = {
				ai_compassion = -0.25
				ai_energy = -0.25
			}
		}
	}
}
