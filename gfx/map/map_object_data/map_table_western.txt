﻿object={
	name="western_tabletop"
	clamp_to_water_level=yes
	render_pass=MapUnderTerrain
	generated_content=no
	layer="map_table_layer_western"
	entity="tabletop_west_basic_entity"
	count=1
	transform="3100 -50 2048 0 0 0 0 5 5 5" #Anbenanr relaced second value (-1) with -50 to fix overlapping

}
object={
	name="western_tabletop_cloth"
	clamp_to_water_level=yes
	render_pass=MapUnderTerrain
	generated_content=no
	layer="map_table_layer_western"
	entity="tabletop_west_basic_tablecloth_entity"
	count=1
	transform="3100 -50 2048 0 0 0 0 5 5 5" #Anbenanr relaced second value (-1) with -50 to fix overlapping

}
object={
	name="western_tabletop_candles_01"
	clamp_to_water_level=yes
	render_pass=MapUnderTerrain
	generated_content=no
	layer="map_table_layer_western"
	entity="tabletop_west_basic_candles_entity"
	count=1
	transform="3100 -50 2048 0 0 0 0 5 5 5" #Anbenanr relaced second value (-1) with -50 to fix overlapping

}
object={
	name="western_tabletop_props_01"
	clamp_to_water_level=yes
	render_pass=MapUnderTerrain
	generated_content=no
	layer="map_table_layer_western"
	entity="tabletop_west_basic_props_entity"
	count=1
	transform="3100 -50 2048 0 0 0 0 5 5 5" #Anbenanr relaced second value (-1) with -50 to fix overlapping

}
