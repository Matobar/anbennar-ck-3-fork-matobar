﻿
# k_castonath = {
# }

d_castonath = {
	1021.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = k_cast # While Cast has the loyalty of the patricians in north castanor, Anor actually has the rest
		holder = 92 #Chlothar of Castonath
		government = republic_government
	}
}

c_north_castonath = {
	1000.1.1 = { change_development_level = 22 }
	1021.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 92	#Chlothar of Castonath
	}
}

c_south_castonath = {
	1000.1.1 = { change_development_level = 25 }
	1021.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = k_anor
		holder = escanni_0009
		government = theocracy_government
	}
}

c_lower_castonath = {
	1000.1.1 = { change_development_level = 20 }
	1021.10.31 = {	#Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = k_anor
		holder = escanni_0010
	    government = republic_government
	}
}
