k_elkmarch = {
	1000.1.1 = {
		change_development_level = 8
	}
	1018.9.15 = {
		holder = 97 #Sybille Locke
	}
}

d_elkmarch = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_elkwood = {
	1018.9.15 = {
		holder = 97 #Sybille Locke
	}
}

c_hardoaks = {
	1000.1.1 = {
		change_development_level = 8
	}
	1018.9.15 = {
		holder = 97 #Sybille Locke
	}
}

c_twofork = {
	1018.9.15 = {
		holder = 97 #Sybille Locke
	}
}

c_the_approach = {
	1018.9.15 = {
		holder = 97 #Sybille Locke
	}
}

d_uelaire = {
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

c_uelaire = {
	1000.1.1 = {
		change_development_level = 9
	}
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

c_westmere = {
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

c_highwind = {
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}

c_newacre = {
	1013.11.7 = {
		holder = 43 #Eustace sil Uelaire
	}
}