k_pearlsedge = {
	1000.1.1 = { change_development_level = 8 }
	879.1.1 = {
		holder = pearlman_0001 # Henrik I "Divenscourge"
	}
	885.11.30 = {
		holder = pearlman_0002 # Haakon I Henriksson
	}
	901.2.19 = {
		holder = pearlman_0006 # Harald Pearlman
	}
	934.7.12 = {
		holder = pearlman_0007 # Ernmund I Pearlman
	}
	947.05.07 = {
		holder = pearlman_0008 # Henrik II Pearlman
	}
	953.05.30 = {
		holder = pearlman_0009 # Ernmund II Pearlman
	}
	971.01.13 = {
		holder = pearlman_0010 # Haakon II Pearlman
	}
	994.03.13 = {
		holder = pearlman_0013 # Aron I Pearlman
	}
}

d_pearlywine = {
	1000.1.1 = { change_development_level = 9 }
}

c_jewelpoint = {
	1000.1.1 = { change_development_level = 9 }
}

c_the_pearls = {
	1000.1.1 = { change_development_level = 6 }
}